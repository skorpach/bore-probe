﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChartFSM9Demo;

namespace FsmInterface
{
    public class FsmInterfacer
    {
        private static Process fsmInterface;
        private static ConcurrentQueue<FSM_9_SensorDataPoint> myOutQueue = new ConcurrentQueue<FSM_9_SensorDataPoint>(), 
            myMagQueue = new ConcurrentQueue<FSM_9_SensorDataPoint>();
        private static String pattern = @"LA\s+x:\s*(?<xval>\-?\d+.\d+\s*),\sy:\s*(?<yval>\-?\d+.\d+\s*),\s*z:\s*(?<zval>\-?\d+.\d+\s*),\s*seq:\s*(?<seq>\d+)\s*";
        private static String magPattern = @"MG\s+x:\s*(?<xval>\-?\d+.\d+\s*),\sy:\s*(?<yval>\-?\d+.\d+\s*),\s*z:\s*(?<zval>\-?\d+.\d+\s*),\s*seq:\s*(?<seq>\d+)\s*";
        private static String ndfPattern = @"INVALID DEVICE ERROR";
        private static String dfPattern = @"DEVICE FOUND";
        private static String procEndPattern = @"PROC END";
        private static Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase),
            ndfexp = new Regex(ndfPattern, RegexOptions.IgnoreCase),
            magrgx = new Regex(magPattern, RegexOptions.IgnoreCase),
            dfexp = new Regex(dfPattern, RegexOptions.IgnoreCase),
            procEndExp = new Regex(procEndPattern, RegexOptions.IgnoreCase);
        private static Stopwatch sw = new Stopwatch();
        private static bool hasDevice = false;

        //Returns next data point in queue.
        public static FSM_9_SensorDataPoint nextData
        {
            get
            {
                FSM_9_SensorDataPoint ret;
                if (myOutQueue.TryDequeue(out ret))
                    return ret;
                return null;
            }
        }

        //Returns the next mag data point
        public static FSM_9_SensorDataPoint nextMag
        {
            get
            {
                FSM_9_SensorDataPoint ret;
                if(myMagQueue.TryDequeue(out ret))
                    return ret;
                return null;
            }
        }

        //Indicates whether the sub process is currently running.
        public static bool running
        {
            get
            {
                return fsmInterface != null && hasDevice;
            }
        }

        public static bool ProcEndEventAttached
        {
            get
            {
                return ProcessEnd != null;
            }
        }

        public static bool DeviceFoundEventAttached
        {
            get
            {
                return DeviceFound != null;
            }
        }

        //Event called when process stops or finds device
        public static event EventHandler ProcessEnd, DeviceFound;

        public static void start()
        {
            fsmInterface = new Process();
            fsmInterface.StartInfo.CreateNoWindow = true;
            fsmInterface.StartInfo.FileName = "motion_example.exe";
            fsmInterface.StartInfo.UseShellExecute = false;
            fsmInterface.StartInfo.RedirectStandardOutput = true;
            fsmInterface.OutputDataReceived += FsmInterface_OutputDataReceived;
            fsmInterface.Start();
            fsmInterface.BeginOutputReadLine();

            hasDevice = false;

            sw.Start();
        }

        public static void stop()
        {
            if (fsmInterface != null)
            {
                if (!fsmInterface.HasExited)
                    fsmInterface.Kill();
            }

            fsmInterface = null;
            sw.Stop();
            sw.Reset();
            hasDevice = false;
        }

        private static void FsmInterface_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            String line = e.Data;
            if (String.IsNullOrEmpty(line)) return;
            //Console.WriteLine(line);
                
            //parse the line and enqueue it when it matches.

            Match m = rgx.Match(line);
            if (m.Value != String.Empty)
            {
                //Console.WriteLine(m.ToString());
                GroupCollection g = m.Groups;
                FSM_9_SensorDataPoint dp = new FSM_9_SensorDataPoint()
                {
                    X = Double.Parse(g["yval"].Value) * 0.10197162129779283f,
                    Y = Double.Parse(g["xval"].Value) * 0.10197162129779283f,
                    Z = Double.Parse(g["zval"].Value) * 0.10197162129779283f * -1.0f,
                    SampleId = int.Parse(g["seq"].Value),
                    TimeStamp = sw.ElapsedMilliseconds
                };
                if (dp != null)
                {
                    myOutQueue.Enqueue(dp);
                    // Console.WriteLine(dp.ToString());
                }

                return;
            }

            //Try extracting mag data instead
            m = magrgx.Match(line);
            if(m.Value != String.Empty)
            {
                GroupCollection g = m.Groups;

                myMagQueue.Enqueue(new FSM_9_SensorDataPoint()
                {
                    X = Double.Parse(g["yval"].Value),
                    Y = Double.Parse(g["xval"].Value),
                    Z = Double.Parse(g["zval"].Value),
                    SampleId = int.Parse(g["seq"].Value),
                    TimeStamp = sw.ElapsedMilliseconds
                });

                return;
            }

            m = procEndExp.Match(line);
            if(m.Value != String.Empty)
            {
                stop();
                ProcessEnd(null, e);
                return;
            }

            m = dfexp.Match(line);
            if(m.Value != String.Empty)
            {
                hasDevice = true;
                DeviceFound(null, e);
                return;
            }

            m = ndfexp.Match(line);
            if(m.Value != String.Empty)
            {
                stop();
                ProcessEnd(null, e);
                return;
            }
        }
    }

    public class DeviceNotFoundException : Exception
    {
        public DeviceNotFoundException() { }

        public DeviceNotFoundException(String msg) : base(msg) { }
    }
}
