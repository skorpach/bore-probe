﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FsmInterface
{
        public class FSM_9_DataPoint
        {
            public double X { get; set; }
            public double Y { get; set; }
            public double Z { get; set; }

            //microseconds
            public int TimeStamp { get; set; }

            //Key... unique per run
            public int SampleId { get; set; }

            public override String ToString()
            {
                return String.Format("X:\t{0}; Y:\t{1}; Z:\t{2}, SEQ:\t{3}, TIME:\t{4}", X, Y, Z, SampleId, TimeStamp);
            }
        }

        public interface DataPipe
        {
            List<FSM_9_DataPoint> GetGyroData();

            List<FSM_9_DataPoint> GetMagData();
        }
 }
