﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Media.Media3D;

namespace ChartFSM9Demo
{
    public enum Stations { Calibration, Station1, Station2, Station3, Station4 };

    public class FSM_9_SensorDataPoint
    {
        public double X {
            get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public bool calibrated { get; set; } = false;

        public int StationID { get; set; } = 0;
        //Assume this is in microseconds
        public double TimeStamp { get; set; }

        public double TimeInMiliseconds
        {
            get
            {
                return TimeStamp;
            }
        }
        public double TimeInSeconds
        {
            get
            {
                return TimeStamp / 1000;
            }
        }
        public double VectorMagnitude
        {
            get
            {
                //Vector3D v = new Vector3D(X, Y, Z);
                //same result as v.length
                var sum = Math.Pow(X, 2) + Math.Pow(Y, 2) + Math.Pow(Z, 2);
                var length = Math.Sqrt(sum);
                return length;
            }
        }

        public double Inclination
        {
            get
            {
                var radians = Math.Asin(X / VectorMagnitude);
                var degrees = radians * (180 / Math.PI);
                return degrees;
            }
        }
        public int SampleId { get; set; }
        public bool AlreadyReturned { get;  set; }

        public double CalibratedInclination(FSM_9_SensorDataPoint calibrator)
        {
            double dotprod = X * calibrator.X + Y * calibrator.Y + Z * calibrator.Z;
            return Math.Acos(dotprod / (VectorMagnitude * calibrator.VectorMagnitude)) * 180 / Math.PI;
        }

        #region Constructors
        public FSM_9_SensorDataPoint(double x, double y, double z, int timestamp, int sampleId)
        {
            X = x;
            Y = y;
            Z = z;
            TimeStamp = timestamp;
            SampleId = sampleId;
        }
        public FSM_9_SensorDataPoint(string x, string y, string z, string timestamp, string sampleId):
            this(Convert.ToDouble(x), Convert.ToDouble(y), Convert.ToDouble(z), Convert.ToInt32(timestamp), Convert.ToInt32(sampleId))
        {
            
        }

        public FSM_9_SensorDataPoint()
        {
        }

        public FSM_9_SensorDataPoint(double manualX, double manualY, double manualZ)
        {
            X = manualX;
            Y = manualY;
            Z = manualZ;
        }
        #endregion
    }

    public class CalibrationMatrix
    {
        private double[,] matrix;
        private delegate void Assignment();

        public CalibrationMatrix(FSM_9_SensorDataPoint calibrationVector)
        {
            if (calibrationVector == null) throw new ArgumentNullException();
            double[] vect = { calibrationVector.X, calibrationVector.Y, calibrationVector.Z };
            double gamma = 0, beta = 0;
            bool gammaFirst = false;
            Assignment calcBeta = () => { beta = vect[2] == 0 ? Math.PI / 2 : -Math.Atan(vect[0] / vect[2]); }
            ,
                calcGamma = () => { gamma = vect[2] == 0 ? Math.PI / 2 : Math.Atan(vect[1] / vect[2]); };

            if(Math.Abs(vect[0]) > Math.Abs(vect[1]))
            {
                calcBeta();
                vect[0] = Math.Cos(beta) * vect[0] + Math.Sin(beta) * vect[2];
                vect[2] = Math.Cos(beta) * vect[2] - Math.Sin(beta) * vect[0];
                calcGamma();
            }
            else
            {
                gammaFirst = true;
                calcGamma();
                vect[2] = Math.Cos(beta) * vect[2] + Math.Sin(beta) * vect[1];
                vect[1] = Math.Cos(beta) * vect[1] - Math.Sin(beta) * vect[2];
                calcBeta();
            }

            
            double cb = Math.Cos(beta), cy = Math.Cos(gamma), sb = Math.Sin(beta), sy = Math.Sin(gamma);
            matrix = new double[3, 3];

            if (gammaFirst)
            {
                matrix[0, 0] = cb;
                matrix[0, 1] = 0;
                matrix[0, 2] = sb;
                matrix[1, 0] = sy * sb;
                matrix[1, 1] = cy;
                matrix[1, 2] = -cb * sy;
                matrix[2, 0] = -sb * cy;
                matrix[2, 1] = sy;
                matrix[2, 2] = cy * cb;
            }
            else
            {
                matrix[0, 0] = cb;
                matrix[0, 1] = sb * sy;
                matrix[0, 2] = sb * cy;
                matrix[1, 0] = 0;
                matrix[1, 1] = cy;
                matrix[1, 2] = -sy;
                matrix[2, 0] = -sb;
                matrix[2, 1] = sy * cb;
                matrix[2, 2] = cy * cb;
            }
        }

        //Apply matrix to vector for calibration
        public FSM_9_SensorDataPoint calibrateVector(FSM_9_SensorDataPoint vect)
        {
            if (vect == null) return null;

            double[] outVector = { 0, 0, 0 }, inVector = { vect.X, vect.Y, vect.Z };

            for (int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    outVector[i] += matrix[i,j] * inVector[j];
                }
            }

            return new FSM_9_SensorDataPoint()
            {
                X = outVector[0],
                Y = outVector[1],
                Z = outVector[2],
                SampleId = vect.SampleId,
                TimeStamp = vect.TimeStamp,
                calibrated = true
            };
        }
    }
}
