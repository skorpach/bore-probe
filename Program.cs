﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HillcrestLabs.Freespace;


namespace freespace_controls
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Freespace.Init(null) != Freespace.FreespaceError.SUCCESS)
            {
                Console.WriteLine("There was an issue with the freespace library.");
                Console.ReadKey(); 
                return;
            }

            List<FreespaceDevice> deviceList = Freespace.GetDevices();
            Console.WriteLine(deviceList.ToString());

            int i = 0;
            while(deviceList.Count < 1)
            {
                i++;
                Console.WriteLine("Could not find any devices");
                System.Threading.Thread.Sleep(250);
                if (i > 16) return;
                deviceList = Freespace.GetDevices();
            }

            FreespaceDevice dev = deviceList[0];
            if (!dev.Open())
            {
                //.open() true on success.
                Console.WriteLine("Could not open the device for communication.");
            }
            Console.WriteLine("Found a device: " + dev.Name);
            dev.Flush();
            //Enable position reports and quit if the message cannot send.
            if(!dev.SendMessage(new HillcrestLabs.Freespace.FreespaceDataMotionControl
            {
                EnableUserPosition = true
            }))
            {
                Console.WriteLine("ERROR: Failed to send message to device. Fatal.");
                Console.ReadKey();
                return;
            }

            //Get the responses and print them (forever?)
            FreespaceMessage rm = dev.ReadMessage(250);
            Console.WriteLine(rm.ToString());
        }
    }
}
