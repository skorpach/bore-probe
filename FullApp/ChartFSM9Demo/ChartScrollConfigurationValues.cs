﻿using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ChartFSM9Demo
{
    // Based on 120 Hz Average Sample rate, need to configure the timer and how many points are grabbed each time for a smooth display
    // Also, in the future, maybe configure the x-axis to display either current time (datetime), or running time since t=0 starting point.
    public static class ChartScrollConfigurationValues
    {
        public static int ScrollTimerInterval = 100;//microseconds
        //I think this should really be a range based on how many points are returned from the buffer... 
        public static int PointsToGrabPerInterval = 12;// this gives about 10 microseconds per point or about 100 points in a second
        public static float TimeAxis_IEtheXAxis = 5;
        public static int NumberOfPointsInAChart = (int)TimeAxis_IEtheXAxis * 100;
        public static float LinearAccelerationMaxValue_IEtheYAxis = 1.2f;
        public static float LinearAccelerationMinValue_IEtheYAxis = -1.2f;
        public static float InclinationMinValue = 0f;
        public static float InclinationMaxValue = 90f;
    }
}
