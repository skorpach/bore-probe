﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChartFSM9Demo
{
    public partial class DataDetailLog : Form
    {
        private List<FSM_9_SensorDataPoint> data;
        private Stations station;

        public DataDetailLog()
        {
            InitializeComponent();
        }

        public DataDetailLog(Stations station, List<FSM_9_SensorDataPoint> data):this()
        {
            this.station = station;
            this.data = data;
            this.Text = station.ToString() + " Data";
        }
        public DataDetailLog( List<FSM_9_SensorDataPoint> data) : this()
        {
            this.data = data;
            this.Text = "Complete Sample Log";
        }
        private void DataDetailLog_Load(object sender, EventArgs e)
        {
            bindingSource1.DataSource = this.data;
            var foo = station;
        }

        private void buttonCSVExport_Click(object sender, EventArgs e)
        {
            ExportGrid();
        }

        private void ExportGrid()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            //saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.Filter = "csv files (*.csv)|*.csv";
            //saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var sb = new StringBuilder();

                var headers = dataGridView1.Columns.Cast<DataGridViewColumn>();
                headers = headers.Where(h => h.Visible == true);

                sb.AppendLine(string.Join(",", headers.Select(column => "\"" + column.HeaderText + "\"").ToArray()));

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    var cells = row.Cells.Cast<DataGridViewCell>();
                    cells = cells.Where(c => c.Visible == true);
                    sb.AppendLine(string.Join(",", cells.Select(cell => "\"" + cell.Value + "\"").ToArray()));
                }

                File.WriteAllText(saveFileDialog1.FileName, sb.ToString());
            }
        }
    }
}
