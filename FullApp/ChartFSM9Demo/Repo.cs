﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace ChartFSM9Demo
{
    public class Repo
    {
        
        public int StationID { get; set; } = 10;

        public CalibrationMatrix calima = null;

       


        private List<FSM_9_SensorDataPoint> LinearAccelerationData;
        private List<FSM_9_SensorDataPoint> MAGData;

        private Dictionary<Stations, List<FSM_9_SensorDataPoint>> StationDataDictionary;
        private Dictionary<Stations, FSM_9_SensorDataPoint> StationAverages;

        //constructor
        public Repo()
        {
            LinearAccelerationData = new List<FSM_9_SensorDataPoint>();
            MAGData = new List<FSM_9_SensorDataPoint>();
        }

        public void ResetRepo(bool usingSensor)
        {
            LinearAccelerationData = null;
            MAGData = null;
            StationAverages = null;
            StationDataDictionary = null;
        }

        
        public List<FSM_9_SensorDataPoint> GetEntireLALog()
        {
            return LinearAccelerationData;
        }
        //used to graph

        public List<FSM_9_SensorDataPoint> GetLinearAccelerationSensorData()
        {
            var ret = new List<FSM_9_SensorDataPoint>();
            while (true)
            {
                FSM_9_SensorDataPoint dp = FsmInterface.FsmInterfacer.nextData;
                if (dp == null) break;
                dp.StationID = this.StationID;
                ret.Add(dp);
                LinearAccelerationData.Add(dp);
                dp.AlreadyReturned = true;
            }
            return ret;
        }

        public List<FSM_9_SensorDataPoint> GetMagData(int howManyPoints)
        {
            var ret = new List<FSM_9_SensorDataPoint>();
            while (true)
            {
                FSM_9_SensorDataPoint dp = FsmInterface.FsmInterfacer.nextMag;
                if (dp == null) break;
                ret.Add(dp);
                MAGData.Add(dp);
                dp.AlreadyReturned = true;
            }
            return ret;
        }


        #region Station Data
        public List<FSM_9_SensorDataPoint> GetStationData(Stations station)
        {
            return StationDataDictionary[station];
        }

        public void SetStationData(StationSignalAcquisition stationSignalAcquisition)
        {
            Stations currentStation = stationSignalAcquisition.Station;
            List<FSM_9_SensorDataPoint> stationData = stationSignalAcquisition.GetStationData();
            if (StationDataDictionary == null)
            {
                StationDataDictionary = new Dictionary<Stations, List<FSM_9_SensorDataPoint>>();
            }
            if (!StationDataDictionary.ContainsKey(currentStation))
            {
                StationDataDictionary.Add(currentStation, stationData);
            }
            else//overwrite existing station data
            {
                StationDataDictionary[currentStation] = stationData;
            }

        }
        #endregion

        #region Calculating Averages and Calibrated Values

        public FSM_9_SensorDataPoint GetStationAverage(Stations station)
        {

            if (StationDataDictionary != null && StationDataDictionary.ContainsKey(station))
            {
                if (StationAverages == null)
                {
                    StationAverages = new Dictionary<Stations, FSM_9_SensorDataPoint>();
                }
                if (StationAverages.ContainsKey(station))
                {
                    StationAverages.Remove(station);
                }
                FSM_9_SensorDataPoint averagedStationDataPoint = new FSM_9_SensorDataPoint();
                var AverageX = StationDataDictionary[station].Select(s => s.X).Sum() / StationDataDictionary[station].Count;
                var AverageY = StationDataDictionary[station].Select(s => s.Y).Sum() / StationDataDictionary[station].Count;
                var AverageZ = StationDataDictionary[station].Select(s => s.Z).Sum() / StationDataDictionary[station].Count;
                averagedStationDataPoint.X = AverageX;
                averagedStationDataPoint.Y = AverageY;
                averagedStationDataPoint.Z = AverageZ;

                StationAverages.Add(station, averagedStationDataPoint);
            }
            return StationAverages[station];
        }

        public FSM_9_SensorDataPoint GetCalibratedAverage(Stations station)
        {
            FSM_9_SensorDataPoint stationPoint = GetStationAverage(station);
            FSM_9_SensorDataPoint calibrationPoint = GetStationAverage(Stations.Calibration);

            if(calima == null)
            {
                calima = new CalibrationMatrix(calibrationPoint);
            }
            
            return calima.calibrateVector(stationPoint);
        }
        #endregion
    }
}
