﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace ChartFSM9Demo
{
    public enum Stations { Calibration, Station1, Station2, Station3, Station4 };

    public class FSM_9_SensorDataPoint
    {

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        //Assume this is in microseconds
        public double TimeStamp { get; set; }

        public double TimeInMiliseconds
        {
            get
            {
                return TimeStamp / 1000;
            }
        }
        public double TimeInSeconds
        {
            get
            {
                return TimeStamp / 1000000;
            }
        }
        public double VectorMagnitude
        {
            get
            {
                //Vector3D v = new Vector3D(X, Y, Z);
                //same result as v.length
                var sum = Math.Pow(X, 2) + Math.Pow(Y, 2) + Math.Pow(Z, 2);
                var length = Math.Sqrt(sum);


                

                return length;
            }
        }

        public double Inclination
        {
            get
            {
                var radians = Math.Acos(X / VectorMagnitude);
                var degrees = radians * (180 / Math.PI);
                return degrees;
            }
        }
        public int SampleId { get; set; }
        public bool AlreadyReturned { get; internal set; }

        #region Constructors
        public FSM_9_SensorDataPoint(double x, double y, double z, int timestamp, int sampleId)
        {
            X = x;
            Y = y;
            Z = z;
            TimeStamp = timestamp;
            SampleId = sampleId;
        }
        public FSM_9_SensorDataPoint(string x, string y, string z, string timestamp, string sampleId):
            this(Convert.ToDouble(x), Convert.ToDouble(y), Convert.ToDouble(z), Convert.ToInt32(timestamp), Convert.ToInt32(sampleId))
        {
            
        }

        public FSM_9_SensorDataPoint()
        {
        }

        public FSM_9_SensorDataPoint(double manualX, double manualY, double manualZ)
        {
            X = manualX;
            Y = manualY;
            Z = manualZ;
        }
        #endregion
    }

    
}
