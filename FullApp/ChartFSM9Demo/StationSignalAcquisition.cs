﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartFSM9Demo
{
    public class StationSignalAcquisition
    {
        private Stations station;
        private List<FSM_9_SensorDataPoint> StationData;

        public StationSignalAcquisition()
        {
            StationData = new List<FSM_9_SensorDataPoint>();
        }

        public StationSignalAcquisition(Stations station):this()
        {
            this.station = station;
        }

        public Stations Station
        {
            get
            {
                return this.station;
            }

        }

        internal void AcquireSignal(List<FSM_9_SensorDataPoint> accelerationData)
        {
            StationData.AddRange(accelerationData);
        }

        internal List<FSM_9_SensorDataPoint> GetStationData()
        {
            return StationData;
        }
    }
}
