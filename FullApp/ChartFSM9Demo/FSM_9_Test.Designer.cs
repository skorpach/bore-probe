﻿namespace ChartFSM9Demo
{
    partial class FSM_9_Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSM_9_Test));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonExportLog = new System.Windows.Forms.Button();
            this.labelStatusMSG = new System.Windows.Forms.Label();
            this.buttonST4Data = new System.Windows.Forms.Button();
            this.buttonST3Data = new System.Windows.Forms.Button();
            this.buttonST2Data = new System.Windows.Forms.Button();
            this.buttonST1Data = new System.Windows.Forms.Button();
            this.buttonCalData = new System.Windows.Forms.Button();
            this.labelStation4 = new System.Windows.Forms.Label();
            this.labelStation3 = new System.Windows.Forms.Label();
            this.labelStation2 = new System.Windows.Forms.Label();
            this.labelStation1 = new System.Windows.Forms.Label();
            this.labelCalibration = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBoxAccelerationCalibrated = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxLASt4CalibratedMag = new System.Windows.Forms.TextBox();
            this.textBoxLASt4CalibratedZ = new System.Windows.Forms.TextBox();
            this.textBoxLASt3CalibratedMag = new System.Windows.Forms.TextBox();
            this.textBoxLASt4CalibratedY = new System.Windows.Forms.TextBox();
            this.textBoxLASt2CalibratedMag = new System.Windows.Forms.TextBox();
            this.textBoxLASt4CalibratedX = new System.Windows.Forms.TextBox();
            this.textBoxLASt1CalibratedMag = new System.Windows.Forms.TextBox();
            this.textBoxLASt3CalibratedZ = new System.Windows.Forms.TextBox();
            this.textBoxLASt3CalibratedY = new System.Windows.Forms.TextBox();
            this.textBoxLASt3CalibratedX = new System.Windows.Forms.TextBox();
            this.textBoxLASt2CalibratedZ = new System.Windows.Forms.TextBox();
            this.textBoxLASt2CalibratedY = new System.Windows.Forms.TextBox();
            this.textBoxLASt2CalibratedX = new System.Windows.Forms.TextBox();
            this.textBoxLASt1CalibratedZ = new System.Windows.Forms.TextBox();
            this.textBoxLASt1CalibratedY = new System.Windows.Forms.TextBox();
            this.textBoxLASt1CalibratedX = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxLASt4CalibratedInclination = new System.Windows.Forms.TextBox();
            this.textBoxLASt3CalibratedInclination = new System.Windows.Forms.TextBox();
            this.textBoxLASt2CalibratedInclination = new System.Windows.Forms.TextBox();
            this.textBoxLASt1CalibratedInclination = new System.Windows.Forms.TextBox();
            this.groupBoxAcceleration = new System.Windows.Forms.GroupBox();
            this.textBoxLAMagSt4 = new System.Windows.Forms.TextBox();
            this.textBoxLAMagSt3 = new System.Windows.Forms.TextBox();
            this.textBoxLAMagSt2 = new System.Windows.Forms.TextBox();
            this.textBoxLAMagSt1 = new System.Windows.Forms.TextBox();
            this.textBoxLAMagCal = new System.Windows.Forms.TextBox();
            this.labelLinearAccMag = new System.Windows.Forms.Label();
            this.textBoxLASt4Z = new System.Windows.Forms.TextBox();
            this.textBoxLASt4Y = new System.Windows.Forms.TextBox();
            this.textBoxLASt4X = new System.Windows.Forms.TextBox();
            this.textBoxLASt3Z = new System.Windows.Forms.TextBox();
            this.textBoxLASt3Y = new System.Windows.Forms.TextBox();
            this.textBoxLASt3X = new System.Windows.Forms.TextBox();
            this.textBoxLASt2Z = new System.Windows.Forms.TextBox();
            this.textBoxLASt2Y = new System.Windows.Forms.TextBox();
            this.textBoxLASt2X = new System.Windows.Forms.TextBox();
            this.textBoxLASt1Z = new System.Windows.Forms.TextBox();
            this.textBoxLASt1Y = new System.Windows.Forms.TextBox();
            this.textBoxLASt1X = new System.Windows.Forms.TextBox();
            this.textBoxLACalZ = new System.Windows.Forms.TextBox();
            this.textBoxLACalY = new System.Windows.Forms.TextBox();
            this.textBoxLACalX = new System.Windows.Forms.TextBox();
            this.labelLinearAccZ = new System.Windows.Forms.Label();
            this.labelLinearAccY = new System.Windows.Forms.Label();
            this.labelLinearAccX = new System.Windows.Forms.Label();
            this.timerAcquiringSignal = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBoxAccelerationCalibrated.SuspendLayout();
            this.groupBoxAcceleration.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 514F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1128, 514);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBoxAccelerationCalibrated);
            this.panel1.Controls.Add(this.groupBoxAcceleration);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1122, 508);
            this.panel1.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonExportLog);
            this.panel3.Controls.Add(this.labelStatusMSG);
            this.panel3.Controls.Add(this.buttonST4Data);
            this.panel3.Controls.Add(this.buttonST3Data);
            this.panel3.Controls.Add(this.buttonST2Data);
            this.panel3.Controls.Add(this.buttonST1Data);
            this.panel3.Controls.Add(this.buttonCalData);
            this.panel3.Controls.Add(this.labelStation4);
            this.panel3.Controls.Add(this.labelStation3);
            this.panel3.Controls.Add(this.labelStation2);
            this.panel3.Controls.Add(this.labelStation1);
            this.panel3.Controls.Add(this.labelCalibration);
            this.panel3.Location = new System.Drawing.Point(429, 10);
            this.panel3.Margin = new System.Windows.Forms.Padding(1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(176, 176);
            this.panel3.TabIndex = 53;
            // 
            // buttonExportLog
            // 
            this.buttonExportLog.Location = new System.Drawing.Point(123, 33);
            this.buttonExportLog.Name = "buttonExportLog";
            this.buttonExportLog.Size = new System.Drawing.Size(47, 135);
            this.buttonExportLog.TabIndex = 69;
            this.buttonExportLog.TabStop = false;
            this.buttonExportLog.Text = "Export";
            this.buttonExportLog.UseVisualStyleBackColor = true;
            this.buttonExportLog.Click += new System.EventHandler(this.buttonExportLog_Click);
            // 
            // labelStatusMSG
            // 
            this.labelStatusMSG.Location = new System.Drawing.Point(4, 3);
            this.labelStatusMSG.Name = "labelStatusMSG";
            this.labelStatusMSG.Size = new System.Drawing.Size(166, 27);
            this.labelStatusMSG.TabIndex = 68;
            this.labelStatusMSG.Text = "labelStatusMSG";
            this.labelStatusMSG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonST4Data
            // 
            this.buttonST4Data.Location = new System.Drawing.Point(56, 145);
            this.buttonST4Data.Name = "buttonST4Data";
            this.buttonST4Data.Size = new System.Drawing.Size(61, 23);
            this.buttonST4Data.TabIndex = 67;
            this.buttonST4Data.Text = "Acquire";
            this.buttonST4Data.UseVisualStyleBackColor = true;
            this.buttonST4Data.Click += new System.EventHandler(this.buttonST4Data_Click);
            // 
            // buttonST3Data
            // 
            this.buttonST3Data.Location = new System.Drawing.Point(56, 118);
            this.buttonST3Data.Name = "buttonST3Data";
            this.buttonST3Data.Size = new System.Drawing.Size(61, 23);
            this.buttonST3Data.TabIndex = 66;
            this.buttonST3Data.Text = "Acquire";
            this.buttonST3Data.UseVisualStyleBackColor = true;
            this.buttonST3Data.Click += new System.EventHandler(this.buttonST3Data_Click);
            // 
            // buttonST2Data
            // 
            this.buttonST2Data.Location = new System.Drawing.Point(56, 91);
            this.buttonST2Data.Name = "buttonST2Data";
            this.buttonST2Data.Size = new System.Drawing.Size(61, 23);
            this.buttonST2Data.TabIndex = 65;
            this.buttonST2Data.Text = "Acquire";
            this.buttonST2Data.UseVisualStyleBackColor = true;
            this.buttonST2Data.Click += new System.EventHandler(this.buttonST2Data_Click);
            // 
            // buttonST1Data
            // 
            this.buttonST1Data.Location = new System.Drawing.Point(56, 62);
            this.buttonST1Data.Name = "buttonST1Data";
            this.buttonST1Data.Size = new System.Drawing.Size(61, 23);
            this.buttonST1Data.TabIndex = 64;
            this.buttonST1Data.Text = "Acquire";
            this.buttonST1Data.UseVisualStyleBackColor = true;
            this.buttonST1Data.Click += new System.EventHandler(this.buttonST1Data_Click);
            // 
            // buttonCalData
            // 
            this.buttonCalData.Location = new System.Drawing.Point(56, 33);
            this.buttonCalData.Name = "buttonCalData";
            this.buttonCalData.Size = new System.Drawing.Size(61, 23);
            this.buttonCalData.TabIndex = 63;
            this.buttonCalData.Text = "Acquire";
            this.buttonCalData.UseVisualStyleBackColor = true;
            this.buttonCalData.Click += new System.EventHandler(this.buttonCalData_Click);
            // 
            // labelStation4
            // 
            this.labelStation4.AutoSize = true;
            this.labelStation4.Location = new System.Drawing.Point(1, 150);
            this.labelStation4.Name = "labelStation4";
            this.labelStation4.Size = new System.Drawing.Size(49, 13);
            this.labelStation4.TabIndex = 57;
            this.labelStation4.Text = "Station 4";
            // 
            // labelStation3
            // 
            this.labelStation3.AutoSize = true;
            this.labelStation3.Location = new System.Drawing.Point(1, 123);
            this.labelStation3.Name = "labelStation3";
            this.labelStation3.Size = new System.Drawing.Size(49, 13);
            this.labelStation3.TabIndex = 56;
            this.labelStation3.Text = "Station 3";
            // 
            // labelStation2
            // 
            this.labelStation2.AutoSize = true;
            this.labelStation2.Location = new System.Drawing.Point(1, 96);
            this.labelStation2.Name = "labelStation2";
            this.labelStation2.Size = new System.Drawing.Size(49, 13);
            this.labelStation2.TabIndex = 55;
            this.labelStation2.Text = "Station 2";
            // 
            // labelStation1
            // 
            this.labelStation1.AutoSize = true;
            this.labelStation1.Location = new System.Drawing.Point(1, 67);
            this.labelStation1.Name = "labelStation1";
            this.labelStation1.Size = new System.Drawing.Size(49, 13);
            this.labelStation1.TabIndex = 54;
            this.labelStation1.Text = "Station 1";
            // 
            // labelCalibration
            // 
            this.labelCalibration.AutoSize = true;
            this.labelCalibration.Location = new System.Drawing.Point(1, 38);
            this.labelCalibration.Name = "labelCalibration";
            this.labelCalibration.Size = new System.Drawing.Size(59, 13);
            this.labelCalibration.TabIndex = 53;
            this.labelCalibration.Text = "Calibration ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chart1);
            this.groupBox2.Location = new System.Drawing.Point(7, 192);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1111, 308);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Accelerometers - Raw Stream";
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BackColor = System.Drawing.Color.Transparent;
            this.chart1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.chart1.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea2.AxisX.LabelStyle.Format = "#0.0";
            chartArea2.AxisX.Maximum = 1000D;
            chartArea2.AxisX.Minimum = 0D;
            chartArea2.AxisY.Interval = 0.4D;
            chartArea2.AxisY.LabelStyle.Format = "#0.00";
            chartArea2.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea2.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray;
            chartArea2.AxisY.Maximum = 1.2D;
            chartArea2.AxisY.MaximumAutoSize = 5F;
            chartArea2.AxisY.Minimum = -1.2D;
            chartArea2.AxisY.MinorGrid.Enabled = true;
            chartArea2.AxisY.MinorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea2.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea2.AxisY.MinorTickMark.Enabled = true;
            chartArea2.AxisY.MinorTickMark.LineColor = System.Drawing.Color.Silver;
            chartArea2.AxisY.TitleAlignment = System.Drawing.StringAlignment.Near;
            chartArea2.AxisY2.MajorGrid.Enabled = false;
            chartArea2.BackColor = System.Drawing.Color.WhiteSmoke;
            chartArea2.BackSecondaryColor = System.Drawing.Color.White;
            chartArea2.BorderColor = System.Drawing.Color.DimGray;
            chartArea2.Name = "ChartArea1";
            chartArea2.Position.Auto = false;
            chartArea2.Position.Height = 90F;
            chartArea2.Position.Width = 100F;
            chartArea2.Position.Y = 10F;
            chartArea2.ShadowColor = System.Drawing.Color.Transparent;
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Alignment = System.Drawing.StringAlignment.Center;
            legend2.BackColor = System.Drawing.Color.Transparent;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend2.HeaderSeparatorColor = System.Drawing.Color.Bisque;
            legend2.IsDockedInsideChartArea = false;
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(-21, 0);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chart1.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.Lime,
        System.Drawing.Color.Blue};
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "Legend1";
            series5.Name = "X";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Legend = "Legend1";
            series6.Name = "Y";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Legend = "Legend1";
            series7.Name = "Z";
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Color = System.Drawing.Color.Magenta;
            series8.Legend = "Legend1";
            series8.Name = "Inc.";
            series8.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            this.chart1.Series.Add(series5);
            this.chart1.Series.Add(series6);
            this.chart1.Series.Add(series7);
            this.chart1.Series.Add(series8);
            this.chart1.Size = new System.Drawing.Size(1137, 313);
            this.chart1.TabIndex = 0;
            this.chart1.TabStop = false;
            this.chart1.Text = "chart1";
            // 
            // groupBoxAccelerationCalibrated
            // 
            this.groupBoxAccelerationCalibrated.Controls.Add(this.label1);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.label2);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.label3);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.label4);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt4CalibratedMag);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt4CalibratedZ);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt3CalibratedMag);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt4CalibratedY);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt2CalibratedMag);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt4CalibratedX);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt1CalibratedMag);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt3CalibratedZ);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt3CalibratedY);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt3CalibratedX);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt2CalibratedZ);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt2CalibratedY);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt2CalibratedX);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt1CalibratedZ);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt1CalibratedY);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt1CalibratedX);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.label10);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt4CalibratedInclination);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt3CalibratedInclination);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt2CalibratedInclination);
            this.groupBoxAccelerationCalibrated.Controls.Add(this.textBoxLASt1CalibratedInclination);
            this.groupBoxAccelerationCalibrated.Location = new System.Drawing.Point(609, 10);
            this.groupBoxAccelerationCalibrated.Name = "groupBoxAccelerationCalibrated";
            this.groupBoxAccelerationCalibrated.Size = new System.Drawing.Size(509, 176);
            this.groupBoxAccelerationCalibrated.TabIndex = 27;
            this.groupBoxAccelerationCalibrated.TabStop = false;
            this.groupBoxAccelerationCalibrated.Text = "Accelerometers - Calibrated";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(321, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "GTotal";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "Z";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(132, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 44;
            this.label3.Tag = "";
            this.label3.Text = "Y";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "X";
            // 
            // textBoxLASt4CalibratedMag
            // 
            this.textBoxLASt4CalibratedMag.Location = new System.Drawing.Point(303, 147);
            this.textBoxLASt4CalibratedMag.Name = "textBoxLASt4CalibratedMag";
            this.textBoxLASt4CalibratedMag.ReadOnly = true;
            this.textBoxLASt4CalibratedMag.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt4CalibratedMag.TabIndex = 38;
            this.textBoxLASt4CalibratedMag.TabStop = false;
            // 
            // textBoxLASt4CalibratedZ
            // 
            this.textBoxLASt4CalibratedZ.Location = new System.Drawing.Point(204, 147);
            this.textBoxLASt4CalibratedZ.Name = "textBoxLASt4CalibratedZ";
            this.textBoxLASt4CalibratedZ.ReadOnly = true;
            this.textBoxLASt4CalibratedZ.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt4CalibratedZ.TabIndex = 26;
            this.textBoxLASt4CalibratedZ.TabStop = false;
            // 
            // textBoxLASt3CalibratedMag
            // 
            this.textBoxLASt3CalibratedMag.Location = new System.Drawing.Point(303, 119);
            this.textBoxLASt3CalibratedMag.Name = "textBoxLASt3CalibratedMag";
            this.textBoxLASt3CalibratedMag.ReadOnly = true;
            this.textBoxLASt3CalibratedMag.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt3CalibratedMag.TabIndex = 37;
            this.textBoxLASt3CalibratedMag.TabStop = false;
            // 
            // textBoxLASt4CalibratedY
            // 
            this.textBoxLASt4CalibratedY.Location = new System.Drawing.Point(105, 147);
            this.textBoxLASt4CalibratedY.Name = "textBoxLASt4CalibratedY";
            this.textBoxLASt4CalibratedY.ReadOnly = true;
            this.textBoxLASt4CalibratedY.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt4CalibratedY.TabIndex = 25;
            this.textBoxLASt4CalibratedY.TabStop = false;
            // 
            // textBoxLASt2CalibratedMag
            // 
            this.textBoxLASt2CalibratedMag.Location = new System.Drawing.Point(303, 91);
            this.textBoxLASt2CalibratedMag.Name = "textBoxLASt2CalibratedMag";
            this.textBoxLASt2CalibratedMag.ReadOnly = true;
            this.textBoxLASt2CalibratedMag.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt2CalibratedMag.TabIndex = 36;
            this.textBoxLASt2CalibratedMag.TabStop = false;
            // 
            // textBoxLASt4CalibratedX
            // 
            this.textBoxLASt4CalibratedX.Location = new System.Drawing.Point(6, 147);
            this.textBoxLASt4CalibratedX.Name = "textBoxLASt4CalibratedX";
            this.textBoxLASt4CalibratedX.ReadOnly = true;
            this.textBoxLASt4CalibratedX.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt4CalibratedX.TabIndex = 24;
            this.textBoxLASt4CalibratedX.TabStop = false;
            // 
            // textBoxLASt1CalibratedMag
            // 
            this.textBoxLASt1CalibratedMag.Location = new System.Drawing.Point(303, 63);
            this.textBoxLASt1CalibratedMag.Name = "textBoxLASt1CalibratedMag";
            this.textBoxLASt1CalibratedMag.ReadOnly = true;
            this.textBoxLASt1CalibratedMag.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt1CalibratedMag.TabIndex = 35;
            this.textBoxLASt1CalibratedMag.TabStop = false;
            // 
            // textBoxLASt3CalibratedZ
            // 
            this.textBoxLASt3CalibratedZ.Location = new System.Drawing.Point(204, 119);
            this.textBoxLASt3CalibratedZ.Name = "textBoxLASt3CalibratedZ";
            this.textBoxLASt3CalibratedZ.ReadOnly = true;
            this.textBoxLASt3CalibratedZ.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt3CalibratedZ.TabIndex = 23;
            this.textBoxLASt3CalibratedZ.TabStop = false;
            // 
            // textBoxLASt3CalibratedY
            // 
            this.textBoxLASt3CalibratedY.Location = new System.Drawing.Point(105, 119);
            this.textBoxLASt3CalibratedY.Name = "textBoxLASt3CalibratedY";
            this.textBoxLASt3CalibratedY.ReadOnly = true;
            this.textBoxLASt3CalibratedY.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt3CalibratedY.TabIndex = 22;
            this.textBoxLASt3CalibratedY.TabStop = false;
            // 
            // textBoxLASt3CalibratedX
            // 
            this.textBoxLASt3CalibratedX.Location = new System.Drawing.Point(6, 119);
            this.textBoxLASt3CalibratedX.Name = "textBoxLASt3CalibratedX";
            this.textBoxLASt3CalibratedX.ReadOnly = true;
            this.textBoxLASt3CalibratedX.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt3CalibratedX.TabIndex = 21;
            this.textBoxLASt3CalibratedX.TabStop = false;
            // 
            // textBoxLASt2CalibratedZ
            // 
            this.textBoxLASt2CalibratedZ.Location = new System.Drawing.Point(204, 91);
            this.textBoxLASt2CalibratedZ.Name = "textBoxLASt2CalibratedZ";
            this.textBoxLASt2CalibratedZ.ReadOnly = true;
            this.textBoxLASt2CalibratedZ.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt2CalibratedZ.TabIndex = 20;
            this.textBoxLASt2CalibratedZ.TabStop = false;
            // 
            // textBoxLASt2CalibratedY
            // 
            this.textBoxLASt2CalibratedY.Location = new System.Drawing.Point(105, 91);
            this.textBoxLASt2CalibratedY.Name = "textBoxLASt2CalibratedY";
            this.textBoxLASt2CalibratedY.ReadOnly = true;
            this.textBoxLASt2CalibratedY.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt2CalibratedY.TabIndex = 19;
            this.textBoxLASt2CalibratedY.TabStop = false;
            // 
            // textBoxLASt2CalibratedX
            // 
            this.textBoxLASt2CalibratedX.Location = new System.Drawing.Point(6, 91);
            this.textBoxLASt2CalibratedX.Name = "textBoxLASt2CalibratedX";
            this.textBoxLASt2CalibratedX.ReadOnly = true;
            this.textBoxLASt2CalibratedX.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt2CalibratedX.TabIndex = 18;
            this.textBoxLASt2CalibratedX.TabStop = false;
            // 
            // textBoxLASt1CalibratedZ
            // 
            this.textBoxLASt1CalibratedZ.Location = new System.Drawing.Point(204, 63);
            this.textBoxLASt1CalibratedZ.Name = "textBoxLASt1CalibratedZ";
            this.textBoxLASt1CalibratedZ.ReadOnly = true;
            this.textBoxLASt1CalibratedZ.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt1CalibratedZ.TabIndex = 17;
            this.textBoxLASt1CalibratedZ.TabStop = false;
            // 
            // textBoxLASt1CalibratedY
            // 
            this.textBoxLASt1CalibratedY.Location = new System.Drawing.Point(105, 63);
            this.textBoxLASt1CalibratedY.Name = "textBoxLASt1CalibratedY";
            this.textBoxLASt1CalibratedY.ReadOnly = true;
            this.textBoxLASt1CalibratedY.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt1CalibratedY.TabIndex = 16;
            this.textBoxLASt1CalibratedY.TabStop = false;
            // 
            // textBoxLASt1CalibratedX
            // 
            this.textBoxLASt1CalibratedX.Location = new System.Drawing.Point(6, 63);
            this.textBoxLASt1CalibratedX.Name = "textBoxLASt1CalibratedX";
            this.textBoxLASt1CalibratedX.ReadOnly = true;
            this.textBoxLASt1CalibratedX.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt1CalibratedX.TabIndex = 15;
            this.textBoxLASt1CalibratedX.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(401, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 36;
            this.label10.Text = "Inclination (Deg.)";
            // 
            // textBoxLASt4CalibratedInclination
            // 
            this.textBoxLASt4CalibratedInclination.Location = new System.Drawing.Point(402, 147);
            this.textBoxLASt4CalibratedInclination.Name = "textBoxLASt4CalibratedInclination";
            this.textBoxLASt4CalibratedInclination.ReadOnly = true;
            this.textBoxLASt4CalibratedInclination.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt4CalibratedInclination.TabIndex = 42;
            this.textBoxLASt4CalibratedInclination.TabStop = false;
            // 
            // textBoxLASt3CalibratedInclination
            // 
            this.textBoxLASt3CalibratedInclination.Location = new System.Drawing.Point(402, 119);
            this.textBoxLASt3CalibratedInclination.Name = "textBoxLASt3CalibratedInclination";
            this.textBoxLASt3CalibratedInclination.ReadOnly = true;
            this.textBoxLASt3CalibratedInclination.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt3CalibratedInclination.TabIndex = 41;
            this.textBoxLASt3CalibratedInclination.TabStop = false;
            // 
            // textBoxLASt2CalibratedInclination
            // 
            this.textBoxLASt2CalibratedInclination.Location = new System.Drawing.Point(402, 91);
            this.textBoxLASt2CalibratedInclination.Name = "textBoxLASt2CalibratedInclination";
            this.textBoxLASt2CalibratedInclination.ReadOnly = true;
            this.textBoxLASt2CalibratedInclination.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt2CalibratedInclination.TabIndex = 40;
            this.textBoxLASt2CalibratedInclination.TabStop = false;
            // 
            // textBoxLASt1CalibratedInclination
            // 
            this.textBoxLASt1CalibratedInclination.Location = new System.Drawing.Point(402, 62);
            this.textBoxLASt1CalibratedInclination.Name = "textBoxLASt1CalibratedInclination";
            this.textBoxLASt1CalibratedInclination.ReadOnly = true;
            this.textBoxLASt1CalibratedInclination.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt1CalibratedInclination.TabIndex = 39;
            this.textBoxLASt1CalibratedInclination.TabStop = false;
            // 
            // groupBoxAcceleration
            // 
            this.groupBoxAcceleration.Controls.Add(this.textBoxLAMagSt4);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLAMagSt3);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLAMagSt2);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLAMagSt1);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLAMagCal);
            this.groupBoxAcceleration.Controls.Add(this.labelLinearAccMag);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt4Z);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt4Y);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt4X);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt3Z);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt3Y);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt3X);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt2Z);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt2Y);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt2X);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt1Z);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt1Y);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLASt1X);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLACalZ);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLACalY);
            this.groupBoxAcceleration.Controls.Add(this.textBoxLACalX);
            this.groupBoxAcceleration.Controls.Add(this.labelLinearAccZ);
            this.groupBoxAcceleration.Controls.Add(this.labelLinearAccY);
            this.groupBoxAcceleration.Controls.Add(this.labelLinearAccX);
            this.groupBoxAcceleration.Location = new System.Drawing.Point(7, 10);
            this.groupBoxAcceleration.Name = "groupBoxAcceleration";
            this.groupBoxAcceleration.Size = new System.Drawing.Size(417, 176);
            this.groupBoxAcceleration.TabIndex = 11;
            this.groupBoxAcceleration.TabStop = false;
            this.groupBoxAcceleration.Text = "Accelerometers - Raw";
            // 
            // textBoxLAMagSt4
            // 
            this.textBoxLAMagSt4.Location = new System.Drawing.Point(303, 147);
            this.textBoxLAMagSt4.Name = "textBoxLAMagSt4";
            this.textBoxLAMagSt4.ReadOnly = true;
            this.textBoxLAMagSt4.Size = new System.Drawing.Size(92, 20);
            this.textBoxLAMagSt4.TabIndex = 32;
            this.textBoxLAMagSt4.TabStop = false;
            // 
            // textBoxLAMagSt3
            // 
            this.textBoxLAMagSt3.Location = new System.Drawing.Point(303, 119);
            this.textBoxLAMagSt3.Name = "textBoxLAMagSt3";
            this.textBoxLAMagSt3.ReadOnly = true;
            this.textBoxLAMagSt3.Size = new System.Drawing.Size(92, 20);
            this.textBoxLAMagSt3.TabIndex = 31;
            this.textBoxLAMagSt3.TabStop = false;
            // 
            // textBoxLAMagSt2
            // 
            this.textBoxLAMagSt2.Location = new System.Drawing.Point(303, 91);
            this.textBoxLAMagSt2.Name = "textBoxLAMagSt2";
            this.textBoxLAMagSt2.ReadOnly = true;
            this.textBoxLAMagSt2.Size = new System.Drawing.Size(92, 20);
            this.textBoxLAMagSt2.TabIndex = 30;
            this.textBoxLAMagSt2.TabStop = false;
            // 
            // textBoxLAMagSt1
            // 
            this.textBoxLAMagSt1.Location = new System.Drawing.Point(303, 63);
            this.textBoxLAMagSt1.Name = "textBoxLAMagSt1";
            this.textBoxLAMagSt1.ReadOnly = true;
            this.textBoxLAMagSt1.Size = new System.Drawing.Size(92, 20);
            this.textBoxLAMagSt1.TabIndex = 29;
            this.textBoxLAMagSt1.TabStop = false;
            // 
            // textBoxLAMagCal
            // 
            this.textBoxLAMagCal.Location = new System.Drawing.Point(303, 35);
            this.textBoxLAMagCal.Name = "textBoxLAMagCal";
            this.textBoxLAMagCal.ReadOnly = true;
            this.textBoxLAMagCal.Size = new System.Drawing.Size(92, 20);
            this.textBoxLAMagCal.TabIndex = 28;
            this.textBoxLAMagCal.TabStop = false;
            // 
            // labelLinearAccMag
            // 
            this.labelLinearAccMag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelLinearAccMag.AutoSize = true;
            this.labelLinearAccMag.Location = new System.Drawing.Point(331, 20);
            this.labelLinearAccMag.Name = "labelLinearAccMag";
            this.labelLinearAccMag.Size = new System.Drawing.Size(39, 13);
            this.labelLinearAccMag.TabIndex = 27;
            this.labelLinearAccMag.Text = "GTotal";
            this.labelLinearAccMag.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxLASt4Z
            // 
            this.textBoxLASt4Z.Location = new System.Drawing.Point(204, 147);
            this.textBoxLASt4Z.Name = "textBoxLASt4Z";
            this.textBoxLASt4Z.ReadOnly = true;
            this.textBoxLASt4Z.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt4Z.TabIndex = 26;
            this.textBoxLASt4Z.TabStop = false;
            // 
            // textBoxLASt4Y
            // 
            this.textBoxLASt4Y.Location = new System.Drawing.Point(105, 147);
            this.textBoxLASt4Y.Name = "textBoxLASt4Y";
            this.textBoxLASt4Y.ReadOnly = true;
            this.textBoxLASt4Y.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt4Y.TabIndex = 25;
            this.textBoxLASt4Y.TabStop = false;
            // 
            // textBoxLASt4X
            // 
            this.textBoxLASt4X.Location = new System.Drawing.Point(6, 147);
            this.textBoxLASt4X.Name = "textBoxLASt4X";
            this.textBoxLASt4X.ReadOnly = true;
            this.textBoxLASt4X.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt4X.TabIndex = 24;
            this.textBoxLASt4X.TabStop = false;
            // 
            // textBoxLASt3Z
            // 
            this.textBoxLASt3Z.Location = new System.Drawing.Point(204, 119);
            this.textBoxLASt3Z.Name = "textBoxLASt3Z";
            this.textBoxLASt3Z.ReadOnly = true;
            this.textBoxLASt3Z.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt3Z.TabIndex = 23;
            this.textBoxLASt3Z.TabStop = false;
            // 
            // textBoxLASt3Y
            // 
            this.textBoxLASt3Y.Location = new System.Drawing.Point(105, 119);
            this.textBoxLASt3Y.Name = "textBoxLASt3Y";
            this.textBoxLASt3Y.ReadOnly = true;
            this.textBoxLASt3Y.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt3Y.TabIndex = 22;
            this.textBoxLASt3Y.TabStop = false;
            // 
            // textBoxLASt3X
            // 
            this.textBoxLASt3X.Location = new System.Drawing.Point(6, 119);
            this.textBoxLASt3X.Name = "textBoxLASt3X";
            this.textBoxLASt3X.ReadOnly = true;
            this.textBoxLASt3X.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt3X.TabIndex = 21;
            this.textBoxLASt3X.TabStop = false;
            // 
            // textBoxLASt2Z
            // 
            this.textBoxLASt2Z.Location = new System.Drawing.Point(204, 91);
            this.textBoxLASt2Z.Name = "textBoxLASt2Z";
            this.textBoxLASt2Z.ReadOnly = true;
            this.textBoxLASt2Z.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt2Z.TabIndex = 20;
            this.textBoxLASt2Z.TabStop = false;
            // 
            // textBoxLASt2Y
            // 
            this.textBoxLASt2Y.Location = new System.Drawing.Point(105, 91);
            this.textBoxLASt2Y.Name = "textBoxLASt2Y";
            this.textBoxLASt2Y.ReadOnly = true;
            this.textBoxLASt2Y.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt2Y.TabIndex = 19;
            this.textBoxLASt2Y.TabStop = false;
            // 
            // textBoxLASt2X
            // 
            this.textBoxLASt2X.Location = new System.Drawing.Point(6, 91);
            this.textBoxLASt2X.Name = "textBoxLASt2X";
            this.textBoxLASt2X.ReadOnly = true;
            this.textBoxLASt2X.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt2X.TabIndex = 18;
            this.textBoxLASt2X.TabStop = false;
            // 
            // textBoxLASt1Z
            // 
            this.textBoxLASt1Z.Location = new System.Drawing.Point(204, 63);
            this.textBoxLASt1Z.Name = "textBoxLASt1Z";
            this.textBoxLASt1Z.ReadOnly = true;
            this.textBoxLASt1Z.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt1Z.TabIndex = 17;
            this.textBoxLASt1Z.TabStop = false;
            // 
            // textBoxLASt1Y
            // 
            this.textBoxLASt1Y.Location = new System.Drawing.Point(105, 63);
            this.textBoxLASt1Y.Name = "textBoxLASt1Y";
            this.textBoxLASt1Y.ReadOnly = true;
            this.textBoxLASt1Y.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt1Y.TabIndex = 16;
            this.textBoxLASt1Y.TabStop = false;
            // 
            // textBoxLASt1X
            // 
            this.textBoxLASt1X.Location = new System.Drawing.Point(6, 63);
            this.textBoxLASt1X.Name = "textBoxLASt1X";
            this.textBoxLASt1X.ReadOnly = true;
            this.textBoxLASt1X.Size = new System.Drawing.Size(92, 20);
            this.textBoxLASt1X.TabIndex = 15;
            this.textBoxLASt1X.TabStop = false;
            // 
            // textBoxLACalZ
            // 
            this.textBoxLACalZ.Location = new System.Drawing.Point(204, 35);
            this.textBoxLACalZ.Name = "textBoxLACalZ";
            this.textBoxLACalZ.ReadOnly = true;
            this.textBoxLACalZ.Size = new System.Drawing.Size(92, 20);
            this.textBoxLACalZ.TabIndex = 14;
            this.textBoxLACalZ.TabStop = false;
            // 
            // textBoxLACalY
            // 
            this.textBoxLACalY.Location = new System.Drawing.Point(105, 35);
            this.textBoxLACalY.Name = "textBoxLACalY";
            this.textBoxLACalY.ReadOnly = true;
            this.textBoxLACalY.Size = new System.Drawing.Size(92, 20);
            this.textBoxLACalY.TabIndex = 13;
            this.textBoxLACalY.TabStop = false;
            // 
            // textBoxLACalX
            // 
            this.textBoxLACalX.Location = new System.Drawing.Point(6, 35);
            this.textBoxLACalX.Name = "textBoxLACalX";
            this.textBoxLACalX.ReadOnly = true;
            this.textBoxLACalX.Size = new System.Drawing.Size(92, 20);
            this.textBoxLACalX.TabIndex = 12;
            this.textBoxLACalX.TabStop = false;
            // 
            // labelLinearAccZ
            // 
            this.labelLinearAccZ.AutoSize = true;
            this.labelLinearAccZ.Location = new System.Drawing.Point(242, 20);
            this.labelLinearAccZ.Name = "labelLinearAccZ";
            this.labelLinearAccZ.Size = new System.Drawing.Size(14, 13);
            this.labelLinearAccZ.TabIndex = 2;
            this.labelLinearAccZ.Text = "Z";
            // 
            // labelLinearAccY
            // 
            this.labelLinearAccY.AutoSize = true;
            this.labelLinearAccY.Location = new System.Drawing.Point(142, 20);
            this.labelLinearAccY.Name = "labelLinearAccY";
            this.labelLinearAccY.Size = new System.Drawing.Size(14, 13);
            this.labelLinearAccY.TabIndex = 1;
            this.labelLinearAccY.Tag = "";
            this.labelLinearAccY.Text = "Y";
            // 
            // labelLinearAccX
            // 
            this.labelLinearAccX.AutoSize = true;
            this.labelLinearAccX.Location = new System.Drawing.Point(46, 20);
            this.labelLinearAccX.Name = "labelLinearAccX";
            this.labelLinearAccX.Size = new System.Drawing.Size(14, 13);
            this.labelLinearAccX.TabIndex = 0;
            this.labelLinearAccX.Text = "X";
            // 
            // timerAcquiringSignal
            // 
            this.timerAcquiringSignal.Interval = 1000;
            this.timerAcquiringSignal.Tick += new System.EventHandler(this.timerAcquiringSignal_Tick);
            // 
            // FSM_9_Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1128, 514);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1137, 552);
            this.Name = "FSM_9_Test";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Drillbotics Borehole Survey Utility";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSM_9_Test_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBoxAccelerationCalibrated.ResumeLayout(false);
            this.groupBoxAccelerationCalibrated.PerformLayout();
            this.groupBoxAcceleration.ResumeLayout(false);
            this.groupBoxAcceleration.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Timer timerAcquiringSignal;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonExportLog;
        private System.Windows.Forms.Label labelStatusMSG;
        private System.Windows.Forms.Button buttonST4Data;
        private System.Windows.Forms.Button buttonST3Data;
        private System.Windows.Forms.Button buttonST2Data;
        private System.Windows.Forms.Button buttonST1Data;
        private System.Windows.Forms.Button buttonCalData;
        private System.Windows.Forms.Label labelStation4;
        private System.Windows.Forms.Label labelStation3;
        private System.Windows.Forms.Label labelStation2;
        private System.Windows.Forms.Label labelStation1;
        private System.Windows.Forms.Label labelCalibration;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox groupBoxAccelerationCalibrated;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxLASt4CalibratedMag;
        private System.Windows.Forms.TextBox textBoxLASt4CalibratedZ;
        private System.Windows.Forms.TextBox textBoxLASt3CalibratedMag;
        private System.Windows.Forms.TextBox textBoxLASt4CalibratedY;
        private System.Windows.Forms.TextBox textBoxLASt2CalibratedMag;
        private System.Windows.Forms.TextBox textBoxLASt4CalibratedX;
        private System.Windows.Forms.TextBox textBoxLASt1CalibratedMag;
        private System.Windows.Forms.TextBox textBoxLASt3CalibratedZ;
        private System.Windows.Forms.TextBox textBoxLASt3CalibratedY;
        private System.Windows.Forms.TextBox textBoxLASt3CalibratedX;
        private System.Windows.Forms.TextBox textBoxLASt2CalibratedZ;
        private System.Windows.Forms.TextBox textBoxLASt2CalibratedY;
        private System.Windows.Forms.TextBox textBoxLASt2CalibratedX;
        private System.Windows.Forms.TextBox textBoxLASt1CalibratedZ;
        private System.Windows.Forms.TextBox textBoxLASt1CalibratedY;
        private System.Windows.Forms.TextBox textBoxLASt1CalibratedX;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxLASt4CalibratedInclination;
        private System.Windows.Forms.TextBox textBoxLASt3CalibratedInclination;
        private System.Windows.Forms.TextBox textBoxLASt2CalibratedInclination;
        private System.Windows.Forms.TextBox textBoxLASt1CalibratedInclination;
        private System.Windows.Forms.GroupBox groupBoxAcceleration;
        private System.Windows.Forms.TextBox textBoxLAMagSt4;
        private System.Windows.Forms.TextBox textBoxLAMagSt3;
        private System.Windows.Forms.TextBox textBoxLAMagSt2;
        private System.Windows.Forms.TextBox textBoxLAMagSt1;
        private System.Windows.Forms.TextBox textBoxLAMagCal;
        private System.Windows.Forms.Label labelLinearAccMag;
        private System.Windows.Forms.TextBox textBoxLASt4Z;
        private System.Windows.Forms.TextBox textBoxLASt4Y;
        private System.Windows.Forms.TextBox textBoxLASt4X;
        private System.Windows.Forms.TextBox textBoxLASt3Z;
        private System.Windows.Forms.TextBox textBoxLASt3Y;
        private System.Windows.Forms.TextBox textBoxLASt3X;
        private System.Windows.Forms.TextBox textBoxLASt2Z;
        private System.Windows.Forms.TextBox textBoxLASt2Y;
        private System.Windows.Forms.TextBox textBoxLASt2X;
        private System.Windows.Forms.TextBox textBoxLASt1Z;
        private System.Windows.Forms.TextBox textBoxLASt1Y;
        private System.Windows.Forms.TextBox textBoxLASt1X;
        private System.Windows.Forms.TextBox textBoxLACalZ;
        private System.Windows.Forms.TextBox textBoxLACalY;
        private System.Windows.Forms.TextBox textBoxLACalX;
        private System.Windows.Forms.Label labelLinearAccZ;
        private System.Windows.Forms.Label labelLinearAccY;
        private System.Windows.Forms.Label labelLinearAccX;
    }
}

