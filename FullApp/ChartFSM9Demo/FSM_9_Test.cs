﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using FsmInterface;
using System.Threading;
using System.Diagnostics;

namespace ChartFSM9Demo
{
    public partial class FSM_9_Test : Form
    {
        Repo repo;
        static string LinearAccelerationXAxis = "X";
        static string LinearAccelerationYAxis = "Y";
        static string LinearAccelerationZAxis = "Z";
        static string InclinationSeries = "Inc.";

        static bool restart = false;

        FSM_9_SensorDataPoint calibrator;

        static int PointsToGrabPerInterval = 1000;
        //constructor
        public FSM_9_Test()
        {
            InitializeComponent();    
        }

        #region Initialize Form
        private void Form1_Load(object sender, EventArgs e)
        {
            InitializeDataRepo();
            InitializeUI();
            InitializeChartScrollComponents();
            StartSensor();
        }

        private void InitializeChartScrollComponents()
        {
            timer1.Stop();
            timer1.Interval = ChartScrollConfigurationValues.ScrollTimerInterval;
            chart1.ChartAreas["ChartArea1"].AxisX.Maximum = ChartScrollConfigurationValues.TimeAxis_IEtheXAxis;
            chart1.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
            chart1.ChartAreas["ChartArea1"].AxisY.Maximum = ChartScrollConfigurationValues.LinearAccelerationMaxValue_IEtheYAxis;
            chart1.ChartAreas["ChartArea1"].AxisY.Minimum= ChartScrollConfigurationValues.LinearAccelerationMinValue_IEtheYAxis;
            chart1.ChartAreas["ChartArea1"].AxisY2.Minimum = ChartScrollConfigurationValues.InclinationMinValue;
            chart1.ChartAreas["ChartArea1"].AxisY2.Maximum = ChartScrollConfigurationValues.InclinationMaxValue;
        }

        private void SetStatusLabelTest(string Text)
        {
            labelStatusMSG.Text = Text;
        }

        private void InitializeUI()
        {
            SetStatusLabelTest("");
            buttonExportLog.Text = "Export" + Environment.NewLine + "Data";
            MeasureButtonsEnableDisable(false,
                buttonCalData,
                buttonST1Data,
                buttonST2Data,
                buttonST3Data,
                buttonST4Data
                );
        }

        private void InitializeDataRepo()
        {
            repo = new Repo();
        }
        #endregion

        #region UI button Enable Disable
        private void MeasureButtonsEnableDisable(bool enable, params Button[] btns)
        {
            foreach (var btn in btns)
            {
                btn.Enabled = enable;
            }
        }
        private void StationButtonEnable(Stations station)
        {
            switch (station)
            {
                case Stations.Calibration:
                    MeasureButtonsEnableDisable(true, buttonCalData,  buttonST1Data);
                    break;
                case Stations.Station1:
                    MeasureButtonsEnableDisable(true, buttonCalData, buttonST1Data,  buttonST2Data);
                    break;
                case Stations.Station2:
                    MeasureButtonsEnableDisable(true, buttonCalData, buttonST1Data, buttonST2Data,  buttonST3Data);
                    break;
                case Stations.Station3:
                    MeasureButtonsEnableDisable(true, buttonCalData, buttonST1Data, buttonST2Data, buttonST3Data,  buttonST4Data);
                    break;
                case Stations.Station4:
                    MeasureButtonsEnableDisable(true, buttonCalData, buttonST1Data, buttonST2Data, buttonST3Data, buttonST4Data);
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }
        #endregion

        #region Linear Acceleration Chart draw / update
        private void ScrollLAChart(double lastTimeStamp)
        {
            chart1.ResetAutoValues();
            var chartXMax = chart1.ChartAreas["ChartArea1"].AxisX.Maximum;
            if(lastTimeStamp>chartXMax)
            {
                chart1.ChartAreas["ChartArea1"].AxisX.Maximum = lastTimeStamp;
            }

            while(chart1.Series[LinearAccelerationXAxis].Points.Count > ChartScrollConfigurationValues.NumberOfPointsInAChart)
            {
                chart1.Series[LinearAccelerationXAxis].Points.RemoveAt(0);
                chart1.Series[LinearAccelerationYAxis].Points.RemoveAt(0);
                chart1.Series[LinearAccelerationZAxis].Points.RemoveAt(0);
                chart1.ChartAreas["ChartArea1"].AxisX.Minimum =  chart1.Series[LinearAccelerationZAxis].Points[0].XValue;
            }

            while(chart1.Series[InclinationSeries].Points.Count > ChartScrollConfigurationValues.NumberOfPointsInAChart)
            {
                chart1.Series[InclinationSeries].Points.RemoveAt(0);
            }
        }
        //Called each timer tick
        private void UpdateLAChart()
        {
            var accelerationData = repo.GetLinearAccelerationSensorData();
            if (currentlyAcquiring)
            {
                stationSignalAcquisition.AcquireSignal(accelerationData);
            }
            double lastTimestamp=0;

            foreach (var point in accelerationData)
            {
                chart1.Series[LinearAccelerationXAxis].Points.AddXY(point.TimeInSeconds, point.X);
                chart1.Series[LinearAccelerationYAxis].Points.AddXY(point.TimeInSeconds, point.Y);
                chart1.Series[LinearAccelerationZAxis].Points.AddXY(point.TimeInSeconds, point.Z);
                if(calibrator != null)
                {
                    chart1.Series[InclinationSeries].Points.AddXY(point.TimeInSeconds, point.CalibratedInclination(calibrator));
                }
                lastTimestamp = point.TimeInSeconds;
            }
            ScrollLAChart(lastTimestamp);
            //redraw chart
            chart1.Invalidate();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (restart)
            {
                StartSensor();
                restart = false;
                timer1.Stop();
                return;
            }
            UpdateLAChart();
        }
        #endregion

        #region Show Station measurements

        private void UpdateDisplayedMeasurments(Stations station)
        {
            FSM_9_SensorDataPoint data = repo.GetStationAverage(station);
            FSM_9_SensorDataPoint calibratedData = repo.GetCalibratedAverage(station);
            switch (station)
            {
                case Stations.Calibration:
                    calibrator = data;
                    textBoxLACalX.Text = data.X.ToString();
                    textBoxLACalY.Text = data.Y.ToString();
                    textBoxLACalZ.Text = data.Z.ToString();
                    textBoxLAMagCal.Text = data.VectorMagnitude.ToString();
                    break;
                case Stations.Station1:
                    textBoxLASt1X.Text = data.X.ToString();
                    textBoxLASt1Y.Text = data.Y.ToString();
                    textBoxLASt1Z.Text = data.Z.ToString();
                    textBoxLAMagSt1.Text = data.VectorMagnitude.ToString();
                    
                    textBoxLASt1CalibratedX.Text = calibratedData.X.ToString();
                    textBoxLASt1CalibratedY.Text = calibratedData.Y.ToString();
                    textBoxLASt1CalibratedZ.Text = calibratedData.Z.ToString();
                    textBoxLASt1CalibratedMag.Text = calibratedData.VectorMagnitude.ToString();
                    textBoxLASt1CalibratedInclination.Text = data.CalibratedInclination(calibrator).ToString();
                    
                    break;
                case Stations.Station2:
                    textBoxLASt2X.Text = data.X.ToString();
                    textBoxLASt2Y.Text = data.Y.ToString();
                    textBoxLASt2Z.Text = data.Z.ToString();
                    textBoxLAMagSt2.Text = data.VectorMagnitude.ToString();

                    textBoxLASt2CalibratedX.Text = calibratedData.X.ToString();
                    textBoxLASt2CalibratedY.Text = calibratedData.Y.ToString();
                    textBoxLASt2CalibratedZ.Text = calibratedData.Z.ToString();
                    textBoxLASt2CalibratedMag.Text = calibratedData.VectorMagnitude.ToString();
                    textBoxLASt2CalibratedInclination.Text = data.CalibratedInclination(calibrator).ToString();
                    break;
                case Stations.Station3:
                    textBoxLASt3X.Text = data.X.ToString();
                    textBoxLASt3Y.Text = data.Y.ToString();
                    textBoxLASt3Z.Text = data.Z.ToString();
                    textBoxLAMagSt3.Text = data.VectorMagnitude.ToString();

                    textBoxLASt3CalibratedX.Text = calibratedData.X.ToString();
                    textBoxLASt3CalibratedY.Text = calibratedData.Y.ToString();
                    textBoxLASt3CalibratedZ.Text = calibratedData.Z.ToString();
                    textBoxLASt3CalibratedMag.Text = calibratedData.VectorMagnitude.ToString();
                    textBoxLASt3CalibratedInclination.Text = data.CalibratedInclination(calibrator).ToString();
                    break;
                case Stations.Station4:
                    textBoxLASt4X.Text = data.X.ToString();
                    textBoxLASt4Y.Text = data.Y.ToString();
                    textBoxLASt4Z.Text = data.Z.ToString();
                    textBoxLAMagSt4.Text = data.VectorMagnitude.ToString();

                    textBoxLASt4CalibratedX.Text = calibratedData.X.ToString();
                    textBoxLASt4CalibratedY.Text = calibratedData.Y.ToString();
                    textBoxLASt4CalibratedZ.Text = calibratedData.Z.ToString();
                    textBoxLASt4CalibratedMag.Text = calibratedData.VectorMagnitude.ToString();
                    textBoxLASt4CalibratedInclination.Text = data.CalibratedInclination(calibrator).ToString();
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }

        }
        #endregion

        #region Acquire Station / Calibration Data
        private void buttonCalData_Click(object sender, EventArgs e)
        {
            InitializeUI();
            ResetAllCalculations();
            repo.StationID = 0;
            AcquireStationData(Stations.Calibration);
            MeasureButtonsEnableDisable(false, buttonCalData, buttonST1Data, buttonST2Data, buttonST3Data, buttonST4Data);
        }

        private void DisableWhileAcquiring()
        {
            MeasureButtonsEnableDisable(false, buttonCalData, buttonST1Data, buttonST2Data, buttonST3Data, buttonST4Data);
        }
        private void buttonST1Data_Click(object sender, EventArgs e)
        {
            AcquireStationData(Stations.Station1);
            repo.StationID = 1;
            DisableWhileAcquiring();
        }


        private void buttonST2Data_Click(object sender, EventArgs e)
        {
            AcquireStationData(Stations.Station2);
            repo.StationID = 2;
            DisableWhileAcquiring();
        }

        private void buttonST3Data_Click(object sender, EventArgs e)
        {
            AcquireStationData(Stations.Station3);
            repo.StationID = 3;
            DisableWhileAcquiring();
        }

        private void buttonST4Data_Click(object sender, EventArgs e)
        {
            AcquireStationData(Stations.Station4);
            repo.StationID = 4;
            DisableWhileAcquiring();
        }
        private bool currentlyAcquiring = false;
        private StationSignalAcquisition stationSignalAcquisition;
        private void AcquireStationData(Stations station)
        {
            timerAcquiringSignal.Start();
            SetStatusLabelTest("Acquiring " + station);
            currentlyAcquiring = true;
            stationSignalAcquisition = new StationSignalAcquisition(station);
        }
        int acquiringSignalTotalTime = 0;
        private readonly int DefaultSignalAcquisitionTime = 3;

        private void timerAcquiringSignal_Tick(object sender, EventArgs e)
        {
            acquiringSignalTotalTime++;
            if (acquiringSignalTotalTime == DefaultSignalAcquisitionTime)
            {
                repo.StationID = 10;
                timerAcquiringSignal.Stop();
                SetStatusLabelTest("");
                acquiringSignalTotalTime = 0;
                currentlyAcquiring = false;
                repo.SetStationData(stationSignalAcquisition);
                UpdateDisplayedMeasurments(stationSignalAcquisition.Station);
                StationButtonEnable(stationSignalAcquisition.Station);
                stationSignalAcquisition = null;
                FormatTextTo2Decimals();

            }
        }

        private void FormatTextTo2Decimals()
        {
            List<Control> list = new List<Control>();

            foreach (Control c in groupBoxAcceleration.Controls)
            {
                list.Add(c);
            }
            foreach (Control c in groupBoxAccelerationCalibrated.Controls)
            {
                list.Add(c);
            }

            foreach(Control c in list)
            {
                if (c is TextBox)
                {
                    if (!string.IsNullOrEmpty(((TextBox)c).Text))
                    {
                        ((TextBox)c).Text = string.Format("{0:#,###0.000000}", double.Parse(((TextBox)c).Text));
                    }
                }
            }
        }
        #endregion


        #region Log Export
        private void buttonExportLog_Click(object sender, EventArgs e)
        {
            var data = repo.GetEntireLALog();
            DataDetailLog StationLog = new DataDetailLog( data);
            //not a modal form, center in parent manually
            var x = this.Location.X + this.Size.Width / 2 - StationLog.Size.Width / 2;
            var y = this.Location.Y + this.Size.Height / 2 - StationLog.Size.Height / 2;
            Point p = new Point(x, y);
            StationLog.StartPosition = FormStartPosition.Manual;
            StationLog.Location = p;

            StationLog.Show();
        }

        private void StationLogExport(Stations station)
        {
            var data = repo.GetStationData(station);
            DataDetailLog StationLog = new DataDetailLog(data);
            StationLog.Show();
        }
        #endregion


        private void CloseForm()
        {
            //ProcessCleanup();         //May cause errors in FsmInterfacer if called before FsmInterfacer.stop()
            try
            {
                FsmInterface.FsmInterfacer.stop();
                timer1.Stop();
                
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public void ProcessCleanup()
        {
            try
            {
                var motionExample = Process.GetProcesses().FirstOrDefault(p => p.ProcessName == "motion_example");
                if (motionExample == null) return;
                if (!motionExample.HasExited)
                {
                    motionExample.Kill();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            
        }

        private void FSM_9_Test_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseForm();
        }

        private void StartSensor()
        {
            buttonCalData.Enabled = false;
            
            try
            {
                FsmInterfacer.start();
            }
            catch (DeviceNotFoundException excp)
            {
                MessageBox.Show("Could not connect to FSM");
                Console.WriteLine(excp.Data);
                FsmInterfacer.stop();
            }

            SetStatusLabelTest("Looking for device...");

            if (!FsmInterfacer.DeviceFoundEventAttached)
            {
                FsmInterfacer.DeviceFound += delegate (object s, EventArgs e)
                {
                    labelStatusMSG.Invoke((MethodInvoker)delegate
                    {
                        SetStatusLabelTest("");
                        buttonCalData.Enabled = true;
                        timer1.Start();
                    });
                };
            }

            if (!FsmInterfacer.ProcEndEventAttached)
            {
                FsmInterfacer.ProcessEnd += delegate (Object s, EventArgs e)
                {
                    MessageBox.Show("Lost communication with device");
                    labelStatusMSG.Invoke((MethodInvoker)delegate
                    {
                        ResetEverything();
                        buttonCalData.Enabled = false;
                        StartSensor();
                    });
                };
            }

            repo.calima = null;
        }


        private void ResetEverything()
        {
            timer1.Stop();
            while (chart1.Series[LinearAccelerationXAxis].Points.Count > 0)
            {
                chart1.Series[LinearAccelerationXAxis].Points.RemoveAt(0);
                chart1.Series[LinearAccelerationYAxis].Points.RemoveAt(0);
                chart1.Series[LinearAccelerationZAxis].Points.RemoveAt(0);
            }

            while (chart1.Series[InclinationSeries].Points.Count > 0)
            {
                chart1.Series[InclinationSeries].Points.RemoveAt(0);
            }

                //InitializeChartScrollComponents();
                chart1.ChartAreas["ChartArea1"].AxisX.Maximum = ChartScrollConfigurationValues.TimeAxis_IEtheXAxis;
            chart1.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
            chart1.Invalidate();
            //repo.ResetRepo(!checkBoxUseLog.Checked);
            InitializeUI();
            ResetAllCalculations();
            FsmInterfacer.stop();
        }

        private void ResetAllCalculations()
        {
            textBoxLACalX.Text = null;
            textBoxLACalY.Text = null;
            textBoxLACalZ.Text = null;
            textBoxLAMagCal.Text = null;
            textBoxLASt1X.Text = null;
            textBoxLASt1Y.Text = null;
            textBoxLASt1Z.Text = null;
            textBoxLAMagSt1.Text = null;

            textBoxLASt1CalibratedX.Text = null;
            textBoxLASt1CalibratedY.Text = null;
            textBoxLASt1CalibratedZ.Text = null;
            textBoxLASt1CalibratedMag.Text = null;
            textBoxLASt1CalibratedInclination.Text = null;

            textBoxLASt2X.Text = null;
            textBoxLASt2Y.Text = null;
            textBoxLASt2Z.Text = null;
            textBoxLAMagSt2.Text = null;

            textBoxLASt2CalibratedX.Text = null;
            textBoxLASt2CalibratedY.Text = null;
            textBoxLASt2CalibratedZ.Text = null;
            textBoxLASt2CalibratedMag.Text = null;
            textBoxLASt2CalibratedInclination.Text = null;
            textBoxLASt3X.Text = null;
            textBoxLASt3Y.Text = null;
            textBoxLASt3Z.Text = null;
            textBoxLAMagSt3.Text = null;

            textBoxLASt3CalibratedX.Text = null;
            textBoxLASt3CalibratedY.Text = null;
            textBoxLASt3CalibratedZ.Text = null;
            textBoxLASt3CalibratedMag.Text = null;
            textBoxLASt3CalibratedInclination.Text = null;
            textBoxLASt4X.Text = null;
            textBoxLASt4Y.Text = null;
            textBoxLASt4Z.Text = null;
            textBoxLAMagSt4.Text = null;

            textBoxLASt4CalibratedX.Text = null;
            textBoxLASt4CalibratedY.Text = null;
            textBoxLASt4CalibratedZ.Text = null;
            textBoxLASt4CalibratedMag.Text = null;
            textBoxLASt4CalibratedInclination.Text = null;
        }
    }
}
